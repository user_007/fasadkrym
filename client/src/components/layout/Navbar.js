import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import "./Navbar.css";

class Navbar extends Component {
  render() {
    return (
      <nav className="mb-3 navbar-light">
        <div className="container">
          <Link to="/" className="navbar-brand">
            <span className="brand__logo mr-3"><img src="https://fk-service.ru/img/logo.png" className="mw-100 mh-100" alt="brand" /></span>
            <span className="brand__name">Фасадкрым</span>
          </Link>
          <div className="collapse navbar-collapse">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item active">
                <Link to="/users" className="nav-link">Работники</Link>
              </li>
              <li className="nav-item">
                <Link to="/objects" className="nav-link">Объекты</Link>
              </li>
            </ul>
            <div className="user-account">
              <i className="fa fa-user fa-fw"></i> account
          </div>
          </div>
        </div>
      </nav>
    )
  }
}

export default Navbar;
