import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import "./Footer.css";

class Footer extends Component {
  render() {
    return (
      <div className="footer mt-3">
        <div className="container">
          <div className="row">
            <div className="col-sm-3 footer__section main_section">
              <h3 className="footer__brand">Фасадкрым</h3>
              <p className="brand-slogan">внутренний сервис</p>
              <p className="phone">+7 978 888-19-37</p>
            </div>
            <div className="col-sm-3 footer__section">
              <ul className="site-links">
                <li className="link-list">
                  <Link className="site-link" to="/users">Работники</Link>
                </li>
                <li className="link-list">
                  <Link className="site-link" to="/objects">Наши объекты</Link>
                </li>
              </ul>
            </div>
            <div className="col-sm-3">
              <ul className="site-links">
                  <li className="link-list">
                    <Link className="site-link" to="/">О проекте</Link>
                  </li>
                  <li className="link-list">
                    <Link className="site-link" to="/">Связаться</Link>
                  </li>
                </ul>
            </div>
            <div className="col-sm-2">
              <ul className="site-links">
                  <li className="link-list">
                    <a className="site-link icon_link" href="http://fasadkrym.ru"><i className="fa fa-globe fa-fw"></i></a>
                  </li>
                  <li className="link-list">
                    <Link className="site-link icon_link" to="/"><i className="fab fa-vk fa-fw"></i></Link>
                  </li>
                </ul>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Footer;
