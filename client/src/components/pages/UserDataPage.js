import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import UserComment from '../microcomponents/cards/UserComment';
import Modal from '../common/Modal';
import Textarea from '../common/forms/FormTextArea';
import Select from '../common/forms/FormSelect';

import { formatPhone } from '../../utils'

import { getUserByID, resetUserData, addUserComment } from '../../actions/userActions';

import './UserDataPage.css';

class UserDataPage extends Component {
  state = {
    meta: {
      pageLoading: true,
      openCommentDialog: false,
      loadingNewComment: false
    },
    user: {},
    newComment: {
      commentText: '',
      commentStatus: 'standart'
    }
  }

  static propTypes = {
    getUserByID: PropTypes.func.isRequired,
    addUserComment: PropTypes.func.isRequired,
    users: PropTypes.object.isRequired
  }

  componentDidMount = () => {
    const { match: { params: { userId } }, getUserByID } = this.props;

    getUserByID(userId);
  }

  componentWillUnmount = () => {
    this.props.resetUserData();
  }


  static getDerivedStateFromProps(nextProps, prevState) {
    const { currentUserData, loading, loadingNewComment } = nextProps.users;
    const { meta, user, newComment } = prevState;

    let stateChanges = {}

    if (currentUserData && user !== currentUserData) {
      stateChanges.user = currentUserData;
      stateChanges.meta = {
        ...meta,
        pageLoading: loading
      }
    }

    if (meta.loadingNewComment !== loadingNewComment) {
      if (meta.loadingNewComment) {
        const defaultNewComment = {
          commentText: '',
          commentStatus: 'standart'
        }
  
        if (newComment.commentText.length || newComment.commentStatus !== 'standart') {
          stateChanges.meta = {
            ...meta,
            openCommentDialog: false,
            loadingNewComment: false
          }
          stateChanges.newComment = defaultNewComment;
        }
      } else {
        stateChanges.meta = {
          ...meta,
          loadingNewComment
        }
      }
    }

    if (Object.keys(stateChanges).length) {
      return {
        ...stateChanges
      }
    }

    return null;
  }


  handleCommentChange = e => {
    e.preventDefault();
    const newCommentData = {...this.state.newComment}

    this.setState({
      newComment: {
        ...newCommentData,
        [e.target.name]: e.target.value
      }
    })
  }

  handleOpenCommentModal = e => {
    e.preventDefault();

    this.setState({
      meta: {
        ...this.state.meta,
        openCommentDialog: true
      }
    })
  }

  handleCloseCommentModal = e => {
    e.preventDefault();

    this.setState({
      meta: {
        ...this.state.meta,
        openCommentDialog: false
      }
    })
  }


  handleSubmitNewComment = e => {
    e.preventDefault();
    const { newComment, meta, user: { ourRemarks, _id: userId, ...userData } } = this.state;
    const { addUserComment } = this.props;

    //TODO: create action for saving new comment 

    let { comments } = ourRemarks;

    comments.push({
      text: newComment.commentText,
      status: newComment.commentStatus,
      published: new Date()
    });

    addUserComment(userId, comments);
    
    // this.setState({
    //   newComment: {
    //     commentText: '',
    //     commentStatus: 'standart'
    //   },
    //   meta: {
    //     ...meta,
    //     openCommentDialog: false
    //   },
    //   user: {
    //     ...userData,
    //     ourRemarks: {
    //       ...ourRemarks,
    //       comments
    //     }
    //   }
    // });
  }

  commentStatusOpts = [
    { value: 'standart', text: 'стандартный' },
    { value: 'important', text: 'важный' },
    { value: 'warning', text: 'предупреждение' }
  ]


  render() {
    const { 
      meta: { pageLoading, openCommentDialog }, user, newComment } = this.state;

    if (pageLoading) {
      return <div className="alert alert-info p-5"> Loading... </div>
    }

    const { 
      ourRemarks: { comments },
      photo, firstName, lastName, email, phone, specialization, skills, equipment, info, city
    } = user;

    const { loadingNewComment } = this.props.users

    const commentsContent = comments.length ? 
      comments.map((comment, index) => (
          <UserComment
            key={index}
            text={comment.text}
            status={comment.status}
            published={comment.published}
          />
      )) :
      <Fragment>

        <p className="user-data__text">Заметок не добавлено</p>        

      </Fragment>
      

    return (

      <Fragment>

        <div className="page employee-data_page">

          <div className="page-header">

            <div className="breadcrumbs">хлебные -> крошки -> пользователь</div>
            <h1 className="page-title">Профиль пользователя</h1>

          </div>
          <div className="profile-data">

            <div className="user-profile-data shadow-sm">

              <div className="profile__photo-wrapper">

                <img
                  src={photo}
                  alt={`${firstName} ${lastName}`}
                  className="profile__photo"
                />

              </div>
              <ul className="profile__user-data-list">

                <li className="user-data__item user-data_title">{lastName} {firstName}</li>
                <li className="user-data__item">

                  <i className="fa-fw fa fa-graduation-cap user-data__icon"></i>
                  {specialization}

                </li>
                <li className="user-data__item">

                  <i className="fa-fw fa fa-map-marker-alt user-data__icon"></i>
                  г. {city}

                </li>
                <li className="user-data__item">

                  <i className="fa-fw fa fa-mobile-phone user-data__icon"></i>
                  {formatPhone(phone)}

                </li>
                <li className="user-data__item">

                  <i className="fa-fw fa fa-envelope user-data__icon"></i>
                  {email}

                </li>

              </ul>

            </div>
            <div className="user-profile-info">

              {/* <ul className="profile-block block_profile-navigation">

                <li className="profile-navigation__item">
                
                  <a href="#info" className="profile-navigation__link">Основная</a>

                </li>
                <li className="profile-navigation__item">
                  
                  <a href="#comments" className="profile-navigation__link">Комментарии</a>

                </li>
              
              </ul> */}

              <div className="profile-block block_description shadow-sm">

                <h4 className="profile-block__title">Информация о пользователе</h4>
                <p className="profile-block__text">{info}</p>

              </div>
              <div className="profile-block block_skills shadow-sm">

                <h4 className="profile-block__title">
                  <i className="fa fa-paint-roller mr-2"></i>Навыки:
                </h4>
                <p className="profile-block__text">{skills || 'навыки не были указаны'}</p>

              </div>
              <div className="profile-block block_equipment shadow-sm">

                <h4 className="profile-block__title">
                  <i className="fa fa-hammer mr-2"></i>Оборудование:
                </h4>
                <p className="profile-block__text">{equipment || 'оборудование не было указано'}</p>

              </div>

              <div className="profile-block block_comments shadow-sm">

                <h4 className="profile-block__title">
                  <i className="fa fa-comments mr-2"></i>Заметки:
                </h4>
                <div className="comments-container py-3">
                  {commentsContent}
                </div>
                <button 
                  className="btn btn-outline-secondary btn-xs" 
                  style={{ width: '140px' }}
                  onClick={this.handleOpenCommentModal.bind(this)}
                >
        
                  <i className="fa fa-edit fa-fw mr-1"></i> добавить 
                
                </button>
                <Modal
                  isOpen={openCommentDialog}
                  handleModalClose={this.handleCloseCommentModal}
                >

                  {/* 
                    This is simple comment form for now, 
                    in the future TODO: some advanced features
                    1. type - for highlighting important notes or even stick on top the most important ones
                  */}
                  <form 
                    onSubmit={this.handleSubmitNewComment.bind(this)}
                    className="comment-form py-4"
                  >
                    <Select
                      options={this.commentStatusOpts}
                      label="Тип комментария"
                      name="commentStatus"
                      value={newComment.commentStatus}
                      handleChange={this.handleCommentChange.bind(this)}
                    />
                    <Textarea 
                      label="Заметка"
                      name="commentText"
                      rows={4}
                      info={`добавление короткой заметки, для пользователя ${lastName} ${firstName}`}
                      value={newComment.commentText}
                      handleChange={this.handleCommentChange.bind(this)}
                    />
                    <div className="form-group mt-2">
                    
                      <button 
                        className={`btn btn-info btn-sm pull-right ${loadingNewComment ? 'disabled' : null}`}
                        disabled={loadingNewComment}
                      >
                      
                        {
                          loadingNewComment ? 
                            <i className="fa-fw fa fa-spinner fa-spin"></i> :
                            <i className="fa-fw fa fa-comment mr-1"></i>
                        } добавить
                      
                      </button>
                    
                    </div>

                  </form>
                </Modal>

              </div>

            </div>

          </div>

        </div>

      </Fragment>

    )
  }
}

const mapStateToProps = state => ({
  users: state.users
})

const mapDispatchToProps = {
  getUserByID, resetUserData, addUserComment
}

export default connect(mapStateToProps, mapDispatchToProps)(UserDataPage);
