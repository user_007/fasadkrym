import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { getUserList } from '../../actions/userActions';

import UserCard from '../microcomponents/cards/UserCard';
import Loading from '../common/Loading';
import Alert from '../common/Alert';

import "./UsersPage.css";

class UsersPage extends Component {
  state = {
    meta: {
      pageLoading: true,
      userDataModalIsOpen: false
    },
    userList: [],
    filters: {}
  }

  static propTypes = {
    getUserList: PropTypes.func.isRequired,
    users: PropTypes.object.isRequired
  }

  componentDidMount = () => {
    this.props.getUserList();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { users: { list: nextUserList, loading } } = nextProps;
    const { userList: prevUserList, meta } = prevState;

    let newState = {}

    if (loading !== meta.pageLoading) {
      newState.meta = {
        ...meta,
        pageLoading: loading
      }
    }

    if (nextUserList !== prevUserList) {
      newState.userList = nextUserList;
    }

    if (Object.keys(newState).length) {
      return {
        ...newState
      }
    }

    return null;
  }




  render() {
    const { meta: { pageLoading: loading }, userList } = this.state;

    if (loading) {
      return (
        <div className="page employee_page container">
          <div className="page-loading-wrapper">
            <Loading />
          </div>
        </div>
      )
    }

    const content = userList.length ?
      userList.map(user => (
        <div key={user._id} className="col-sm-6 col-md-4 col-lg-3 card-wrapper">
          <UserCard user={user} />
        </div>        
      )) :
      <Alert
        type="warning" classes="w-100 text-center p-5"
        text="Список пользователей пока что пуст..."
      />

    return (
      <div className="page employee_page container">
        <div className="employee__filter shadow-sm">
          <div className="filter-title">Фильтры и сортировка</div>
          <div className="filter-block">
            <form action="">
              <div className="form-group">
                <label htmlFor="spec">Специальность</label>
                <select name="spec" id="spec">
                  <option value="0">Фасадчик</option>
                  <option value="1">Кровельщик</option>
                  <option value="2">Высотник</option>
                  <option value="3">Подсобник</option>
                </select>
              </div>
              <div className="form-group">
                <label htmlFor="city">Город</label>
                <select name="city" id="city">
                  <option value="0">Севастополь</option>
                  <option value="1">Симферополь</option>
                  <option value="2">Ялта</option>
                  <option value="3">Керчь</option>
                </select>
              </div>
              <div className="form-group">
                <label htmlFor="sort">Метод сортировки</label>
                <select name="sort" id="sort">
                  <option value="0">По Фамилии</option>
                  <option value="1">По телефону</option>
                  <option value="2">По имени</option>
                  <option value="3">По специальности</option>
                </select>
              </div>
            </form>
          </div>
        </div>
        <div className="row card-list">
          {content}
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  users: state.users
})

const mapDispatchToProps = {
  getUserList
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersPage);
