import React, { Component, Fragment } from "react";
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { forEach as _forEach, find as _find } from 'lodash';

import { addNewUser, resetNewUser } from '../../actions/userActions'

import FormFieldFactory from "../common/forms/FormFieldFactory";
import Alert from '../common/Alert';
import Loading from '../common/Loading';

import Modal from '../common/Modal';
import DocumentForm from "../microcomponents/modals/ModalDocumentForm";

import "./ApplicationForm.css";

class ApplicationForm extends Component {
  constructor() {
    super();

    this.state = {
      meta: {
        pageLoading: true,
        documentModal: false,
        redirectDialog: false,
        redirectAllow: false,
        errors: {}
      },  
      photo: { file: null, preview: undefined },
      firstName: "",
      fathersName: "",
      lastName: "",
      specialization: "fasadchik",
      otherSpecialization: "",
      citizenship: "rus",
      otherCitizenship: "",
      skills: "",
      info: "",
      phone: "",
      email: "",
      vk: "",
      city: "sev",
      address: "",
      documents: []
    };

    this.hiddenFields = {
      specialization: React.createRef(),
      citizenship: React.createRef()
    }

    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount = () => {
    //simulate data loading...
    setTimeout(
      this.setState({
        meta: {
          ...this.state.meta,
          pageLoading: false,
          redirectDialog: true
        }
      }),
      5000
    )
  }
  
  // static getDerivedStateFromProps(nextProps, prevState) {
  //   if (nextProps.newUser) {
  //     return {
  //       meta: {
  //         ...prevState,
  //         redirectDialog: true
  //       }
  //     }
  //   }

  //   return null;
  // }

  redirectTimeOut = null;

  componentWillUnmount = () => {
    this.props.resetNewUser();
  }

  onRedirectRequest = () => {
    this.redirectTimeOut = setTimeout(
      () => {
        this.setState({
          meta: {
            ...this.state.meta,
            redirectAllow: true
          }
        })
      }, 5000
    );
  }
  
  onRedirectReset = e => {
    e.preventDefault();

    clearTimeout(this.redirectTimeOut)
  }


  //TODO: if possible, get next lists in Did Mount lifesycle method through ajax request from server
  cityList = [
    { value: "sev", text: "Севастополь" },
    { value: "kerch", text: "Керчь" },
    { value: "alushta", text: "Алушта" },
    { value: "yalta", text: "Ялта" }
  ]

  citizenshipList = [
    { value: "rus", text: "Россия" },
    { value: "ukr", text: "Украина" },
    { value: "belorus", text: "Белоруссия" },
    { value: "kaz", text: "Казахстан" },
    { value: "other", text: "другое..." }
  ]

  specialization = [
    { value: "fasadchik", text: "фасадчик" },
    { value: "krovelshik", text: "кровельщик" },
    { value: "svarshik", text: "сварщик" },
    { value: "beton", text: "бетонщик" }
  ]


  handleChange = e => {
    e.preventDefault();

    const { name, value } = e.target;

    if (name === 'phone') {
      const phonePattern = /^[0-9]{0,11}$/;

      return phonePattern.test(value) ?
        this.setState({ [name]: value }) : null
    }

    if (name === 'citizenship') {
      const { citizenship: { current: otherValueBlock } } = this.hiddenFields;

      if (value === 'other') {
        otherValueBlock.classList.add('show');
      } else {
        otherValueBlock.classList.remove('show');
        return this.setState({
          [name]: value,
          otherCitizenship: ''
        })
      }
    }

    this.setState({
      [name]: value
    });
  };

  handleChangePhoto = ({ file, preview }) => {
    console.log('FILE WITH PREVIEW: ', file, preview);
    this.setState({
      photo: { file, preview }
    })
  }

  handleSubmit = e => {
    e.preventDefault();
    const {
      props: { addNewUser },
      state: { meta, ...fields },
      cityList, citizenshipList, specialization
    } = this;

    let userData = new FormData();;

    _forEach(fields, (val, key) => {

      if (!key.includes('other')) {
        switch (key) {
          case 'specialization':
            userData.append(key, _find(specialization, { value: val }).text);
            break;
          case 'city':
            userData.append(key, _find(cityList, { value: val }).text);
            break;
          case 'citizenship':
            val === 'other' ?
              userData.append(key, fields['otherCitizenship']) :
              userData.append(key, _find(citizenshipList, { value: val }).text);
            break;
          case 'photo':
            userData.append(key, val.file);
            break;
          default:
            userData.append(key, val);
            break;
        }
      }

    })

    console.log('Submited next data: ', userData);
    addNewUser(userData);

  }

  handleDocumentModalClose = () => {
    const { meta: { documentModal, ...metaData } } = this.state;

    if (documentModal) {
      this.setState({
        meta: {
          ...metaData,
          documentModal: false
        }
        
      })
    }
  }

  handleRedirectDialogClose = () => {
    const { meta: { redirectDialog, ...metaData } } = this.state;

    if (redirectDialog) {
      this.setState({
        meta: {
          ...metaData,
          redirectDialog: false
        }
        
      })
    }
  }

  handleDocumentModalOpen = e => {
    e.preventDefault();
    const { meta: { documentModal, ...metaData } } = this.state;
    
    if (!documentModal) {
      this.setState({
        meta: {
          ...metaData,
          documentModal: true
        }
        
      })
    }
  }

  addDocument = documentData => {
    console.log('Here will be process of adding user document...');
    this.setState({
      //change document array in the state
      documentModal: false
    })
  }


  render() {
    console.log('STATE: ', this.state);
    const { meta: { errors, pageLoading, documentModal, redirectDialog, redirectAllow }, ...fields } = this.state;
    const { users: { newUser } } = this.props

    if (redirectAllow) {
      return <Redirect to="/objects" />
    }

    const pageContent = pageLoading ? 
    (
      <div className="page-loading-wrapper">
        <Loading />
      </div>
    ) : 
    (      
      <Fragment>
        <Modal 
          isOpen={redirectDialog}
          onAfterOpen={this.onRedirectRequest}
          handleModalClose={this.handleRedirectDialogClose}
        >
          <div className="container">
            <Alert 
              classes="w-100 text-center m-0" 
              type="warning" 
              text="Перенаправление на страницу списка объектов..." 
            />
            <button 
              className="btn btn-secondary btn-block px-4"
              onClick={this.onRedirectReset}
            >
              Скинуть отсчет перенаправления
            </button>
          </div>
        </Modal>
        <div className="application-form shadow-sm">
          <div className="form-section">
            <div className="application-form__header">
              <h2 className="application-form__title">Данные профиля</h2>
            </div>
          </div>
          <div className="form-section">
            <div className="col-lg-3 col-md-4">
              <FormFieldFactory
                type="file" name="photo" label="Фотография"
                info="фотография не должна превышать размер 400 kb"
                preview={fields.photo.preview} handleChange={this.handleChangePhoto}
                error={errors['photo']}
              />
            </div>
            <div className="col-lg-9 col-md-8 pl-5">
              <FormFieldFactory
                type="input" name="firstName" label="Имя"
                info="необходимо заполнить поле русскими буквами"
                value={fields.firstName} handleChange={this.handleChange}
                error={errors['firstName']}
              />
              <FormFieldFactory
                type="input" name="fathersName" label="Отчество"
                info="необходимо заполнить поле русскими буквами"
                value={fields.fathersName} handleChange={this.handleChange}
                error={errors['fathersName']}
              />
              <FormFieldFactory
                type="input" name="lastName" label="Фамилия"
                info="необходимо заполнить поле русскими буквами"
                value={fields.lastName} handleChange={this.handleChange}
                error={errors['lastName']}
              />
            </div>
          </div>
          <div className="form-section flex-content-space">
            <div className="section__header">
              <h3 className="section__title">
                Контактные данные
              </h3>
              <p className="section__alert alert_info">
                Для того, чтобы мы могли предлагать Вам работу, обязательно укажите свой телефон и адрес эл. почты. Если Вас интересуют только локальные заявки, то укажите, пожалуйста, адрес Вашего проживания
              </p>
            </div>
            <div className="col-md-6">
              <FormFieldFactory
                type="select" name="city" label="Город" options={this.cityList}
                icon="fa-map-marked-alt"
                info="город, в котором Вы проживаете"
                value={fields.city} handleChange={this.handleChange}
              />
            </div>
            <div className="col-md-6">
              <FormFieldFactory
                type="input" name="address" label="Адрес" icon="fa-building"
                info="адрес фактического проживания" placeholder="пример: ул. Ленина 1, квартира 1"
                value={fields.address} handleChange={this.handleChange}
              />
            </div>
            <div className="col-md-6">
              <FormFieldFactory
                type="input" name="phone" label="Телефон" options={this.cityList}
                icon="fa-mobile" placeholder="пример: 79781234567"
                info="укажите телефон для связи (формат: 79781234567)"
                value={fields.phone} handleChange={this.handleChange}
                error={errors['phone']}
              />
            </div>
            <div className="col-md-6">
              <FormFieldFactory
                type="input" name="email" label="Эл. почта" options={this.cityList}
                icon="fa-envelope" placeholder="пример: your@email.com"
                info="эл. почта для отправки уведомлений"
                value={fields.email} handleChange={this.handleChange}
                error={errors['email']}
              />
            </div>
            <div className="col-md-6">
              <FormFieldFactory
                type="input" name="vk" label="Аккаунт ВК" options={this.cityList}
                icon="fa-vk" placeholder="пример: www.vk.com/account-name-or-ID"
                info="Ваш адрес в сети 'Вконтакте'"
                value={fields.vk} handleChange={this.handleChange}
                error={errors['vk']}
              />
            </div>
          </div>
          <div className="form-section flex-content-space">
            <div className="section__header">
              <h3 className="section__title">
                Профессиональные данные
              </h3>
              <p className="section__alert alert_info">
                Укажите Вашу основную специализацию, а также все дополнительные навыки, которыми Вы обладаете. Можете написать чуть более подробную информацию о себе в текстовом поле ниже.
              </p>
            </div>
            <div className="col-md-6 col-sm-8">
              <FormFieldFactory
                type="select" name="specialization" label="Специальность" options={this.specialization}
                info="выберите Вашу специализацию"
                value={fields.specialization} handleChange={this.handleChange}
              />
            </div>
            <div className="col-sm-12">
              <FormFieldFactory
                type="textarea" name="skills" label="Список навыков" rows={2}
                info="Добавьте навыки, которыми Вы обладаете"
                value={fields.skills} handleChange={this.handleChange}
              />
            </div>
            <div className="col-md-12">
              <FormFieldFactory
                type="textarea" name="info" label="Информация о себе" rows={4}
                info="можете написать немного о себе, если хотите"
                value={fields.info} handleChange={this.handleChange}
              />
            </div>
          </div>
          <div className="form-section flex-content-space">
            <div className="section__header">
              <h3 className="section__title">
                Гражданство, документы и сертификаты
              </h3>
              <p className="section__alert alert_info">
                Необходимо указать хотя бы Ваше Гражданство, по возможности заполните графу паспорт. Если у Вас имеются проф сертификаты или допуски, можете их тоже добавить.
              </p>
            </div>
            <div className="col-md-6">
              <FormFieldFactory
                type="select" name="citizenship" label="Гражданство" options={this.citizenshipList}
                info="выберите гражданство из списка"
                value={this.state.citizenship} handleChange={this.handleChange}
              />
            </div>
            <div className="col-md-6 other-value-block" ref={this.hiddenFields.citizenship}>
              <FormFieldFactory
                type="input" name="otherCitizenship" label="другое..."
                info="напишите, какое у Вас гражданство"
                value={this.state.otherCitizenship} handleChange={this.handleChange}
                error={errors['otherCitizenship']}
              />
            </div>
            <div className="col-md-12">
              {/* TODO: create separate component for document input and iterate state.documents to output list of added document here */}
              <Modal
                isOpen={documentModal}
                handleModalClose={this.handleDocumentModalClose}
              >
                <DocumentForm handleApplyDocument={this.addDocument} />
              </Modal>

            </div>
            <div className="col-md-12">
              <div className="control__header">
                <label className="control__title pull-left">Документы и сертификаты </label>
                <p className="control__subtitle">перечень документов спициалиста, а также сертификаты о подтверждении каких-либо навыков</p>
              </div>
              {
                !this.state.documents.length &&
                <Alert
                  type="info"
                  text="Список документов пока пуст, если необходимо что-то добавить нажмите на кнопку ниже"
                />
              }
              <button
                className="btn btn-xs btn-outline-secondary px-4"
                onClick={this.handleDocumentModalOpen.bind(this)}
              >
                <i className="fa fa-document mr-2"></i>Добавить документ
              </button>
            </div>
          </div>
          <hr />
          <div className="form-group row m-0 justify-content-end pt-3 px-3">
            <div className="col">
              <button className="btn btn-block btn-outline-secondary">
                <i className="fa fa-eraser fa-fw mr-3"></i>Очистить форму
              </button>
            </div>
            <div className="col">
              <button className="btn btn-block btn-outline-primary" onClick={this.handleSubmit}>
                <i className="fa fa-user fa-fw mr-3"></i>Добавить специалиста
              </button>
            </div>
          </div>
        </div>

      </Fragment>

    );

    return (
      <div className="container">
        <form>
          <div className="row">{pageContent}</div>

        </form>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  users: state.users
})

const mapDispatchToProps = {
  addNewUser, resetNewUser
}

export default connect(mapStateToProps, mapDispatchToProps)(ApplicationForm);
