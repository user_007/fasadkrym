import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { getListOfObjects } from '../../actions/objectActions'

import Loading from '../common/Loading';
import Alert from '../common/Alert';
import ObjectCard from '../microcomponents/cards/ObjectCard';

import './ObjectPage.css';

class ObjectPage extends Component {
  static propTypes = {
    getListOfObjects: PropTypes.func.isRequired,
    objects: PropTypes.object.isRequired
  }

  state = {
    loading: true,
    objectsList: [],
    filters: {
      //filter by work category, default: all categories, type: select
      category: null, 
      // sort mrthod, default value: by date descending (last first), type: select
      sortMethod: 'date-desc', 
      // filter by years, default value: from all years, types: date-select (calendar)
      yearStart: '2018', 
      yearEnd: new Date().getFullYear(), 
      // filter by presence of photos of the Object: default value: choose all, type: checkbox
      photo: null,
      // number of objects for 1 page, default value: 10, availiable values 10, 20, 30, 50, type: select
      objectsListLimit: 10,
      // object list offset (for page number or 'load more' button), default value: 0
      objectsListOffset: 0
    }
  }

  componentDidMount = () => {
    const { filters } = this.state;
    
    this.props.getListOfObjects(filters);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { objects: { list, loading } } = nextProps;

    let stateChanges = {}

    if (loading !== prevState.loading) {
      stateChanges.loading = loading;
    }

    if (list && list.length) {
      stateChanges.objectsList = list;
    }

    return Object.keys(stateChanges).length ? { ...stateChanges } : null;
  }


  handleApplyFilters = e => {
    const { filters } = this.state;
    e.preventDefault();

    this.props.getListOfObjects(filters);
  }


  render() {
    const { loading, objectsList, filters } = this.state;

    const filterBlock = (
      <div className="employee__filter shadow-sm">

        <div className="filter-title">Фильтры и сортировка</div>


        <form className="filter-block" onSubmit={this.handleApplyFilters.bind(this)}>

        </form>

      </div>
    )
    const content = loading ?
      <Loading /> :
      <Fragment>

        <div className="page-header">

          <div className="breadcrumbs">хлебные -> крошки -> пользователь</div>

          <h1 className="page-title">Список сделанных объектов</h1>

        </div>


        {filterBlock}


        <div className="row card-list">

          {objectsList.length ? objectsList.map(object => (
            <div key={object._id} className="col-sm-6 col-md-4 col-lg-3 card-wrapper">
              <ObjectCard objectData={object} />
            </div>
          )) :
            <Alert classes="w-100" type="info" text="Список объектов пока пуст..." />}

        </div>

      </Fragment>


    return (

      <div className="page object_page">

        <div className="container">{content}</div>

      </div>

    )

  }
}

const mapStateToProps = state => ({
  objects: state.objects
})

const mapDispatchToProps = {
  getListOfObjects
}

export default connect(mapStateToProps, mapDispatchToProps)(ObjectPage);
