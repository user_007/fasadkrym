/**
 * Component with form used for adding data of new Object or
 * editing data of existing object
 * For existing object State Photo should be like this:
 * photo: [{ file: null, preview: '/path/or/url/of/preview.jpg' }]
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import _ from 'lodash';

import CreateField from '../common/forms/FormFieldFactory';
import { getCategoriesList, saveNewObject } from '../../actions/objectActions';

import './NewObjectFormPage.css';


class NewObjectFormPage extends Component {
  static propTypes = {
    getCategoriesList: PropTypes.func.isRequired,
    saveNewObject: PropTypes.func.isRequired
  }

  //TODO: add meta data with loading/pending indicators for various elements
  // start loading, loading photo, pending save object request etc...
  state = {
    errors: {},
    photo: [],
    loading: false,
    title: {
      value: '',
      label: 'Название',
      info: 'в двух словах суть работы',
      icon: 'fa-pencil-alt',
      placeholder: 'название объекта'
    },
    category: {
      value: '0',
      label: 'Категория',
      list: [],
      icon: 'fa-tag',
      info: 'категория выполняемых на объекте работ'
    },
    city: {
      value: '0',
      label: 'Город',
      list: ['Севастополь', 'Симферополь', 'Ялта'],
      icon: 'fa-city',
      info: 'город, где находиться объект'
    },
    address: {
      value: '',
      label: 'Адрес объекта',
      info: 'адрес объекта, например улица и номер дома',
      icon: 'fa-map-marker-alt',
      placeholder: 'местонахождение объекта'
    },
    date: {
      value: '',
      label: 'Дата окончания работ',
      info: 'число, когда объект был закрыт',
      icon: 'fa-calendar-alt',
      placeholder: 'дата окончания работ...'
    },
    description: {
      value: '',
      label: 'Описание',
      info: 'более подробное описание самой работы'
    }
  }

  componentDidMount() {
    this.props.getCategoriesList();
  }

  static getDerivedStateFromProps(props, state) {
    const { objects: { categories, errors: propsErrors } } = props;
    const { category, errors: stateErrors } = state;

    if (categories && state.category.list !== categories) {
      return {
        category: {
          ...category,
          list: categories
        }
      }
    }

    if (propsErrors && propsErrors !== stateErrors) {
      return {
        errors: propsErrors
      }
    }

    return null;
  }

  handleChange = e => {
    const { name, value } = e.currentTarget;

    this.setState({
      [name]: {
        ...this.state[name],
        value
      },
      errors: {
        ...this.state.errors,
        [name]: null
      }
    })
  }

  handleDateChange = date => {
    const value = date.format()

    this.setState({
      date: {
        ...this.state.date,
        value
      }
    })
  }

  handlePhotoUpload = files => {
    this.setState({
      photo: files
    })
  }

  handleFormSubmit = e => {
    e.preventDefault();

    const { errors, photo, loading, ...fields } = this.state;
    const { saveNewObject } = this.props;
    let data = new FormData();
    let existedPhotoes = [];
    let newPhotoesList = [];

    //split all attachments into 2 arrays
    //all existed photoes to check on server side if some of them were removed
    //and other photoes to add to storage
    photo.forEach(item => {
      if (!item.file) {
        existedPhotoes.push(item.preview)
      } else {
        newPhotoesList.push(item.file)
      }
    });

    if (existedPhotoes.length) data.append('existedPhotoes', existedPhotoes);

    if (newPhotoesList.length) {
      newPhotoesList.forEach((file, i) => {
        data.append(`file${i}`, file);
      });
    };

    _.forEach(fields, (field, key) => {
      const formdataFieldValue = field.list ? field.list[field.value] : field.value;
      data.append(key, formdataFieldValue);
    });

    saveNewObject(data);
  }

  render() {
    const { errors, photo, loading, ...fields } = this.state;

    const formFields = _.map(
      fields, (body, key) => {

        let props = {
          ...body,
          name: key,
          error: errors[key],
          type: 'input',
          handleChange: this.handleChange.bind(this)

        }

        if (key === 'category' || key === 'city') {
          props.type = 'select';
          props.loading = !body.list.length;
          props.options = body.list.map((item, index) => {
            return { value: index, text: item }
          })
        }

        if (key === 'date') {
          props.type = 'date';
          props.value = body.value;
          props.handleChange = this.handleDateChange.bind(this);
        }

        if (key === 'description') {
          props.type = 'textarea';
          props.rows = '6';
        }

        const field = (
          <div className="form-group" key={key}>
            <CreateField {...props} />
          </div>
        )

        return field;
      }
    )

    return (
      <div className="page new-object_page">
        <div className="container">
          <div className="object-form shadow-sm">
            <div class="section__header">
              <h3 class="section__title">Новый Объект</h3>
              <p class="section__alert alert_info d-block">Форма для добавления информации о работах на частном объекте. Необходимо обязательно заполнить название объекта, адрес и дату окончания работ. После отправки данных, карточка объекта появиться на странице <Link to="/objects">Наши объекты</Link></p>
            </div>
            <form className="row" onSubmit={this.handleFormSubmit}>
              <div className="col-lg-4 col-md-5">
                <div className="form-group">
                  <CreateField
                    files={photo}
                    name="photo" label="Фото объекта" info="можно загрузить до 12 фотографий"
                    type="multi-file"
                    handleChange={this.handlePhotoUpload.bind(this)}
                  />
                </div>
              </div>
              <div className="col-md-7 col-lg-8">{formFields}</div>
              <div className="col-lg-12 text-right py-3">
                <button type="submit" className="btn btn-outline-secondary px-5 text-center float-right">
                  <i className="fa fa-building mr-2"></i> добавить объект
                  </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  objects: state.objects
})

const mapDispatchToProps = {
  getCategoriesList, saveNewObject
}


export default connect(mapStateToProps, mapDispatchToProps)(NewObjectFormPage);
