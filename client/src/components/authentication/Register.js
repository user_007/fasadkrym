import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import _ from "lodash";
import { Link } from "react-router-dom";

import FormFieldFactory from "../common/forms/FormFieldFactory";
// import Alert from "../common/Alert";
import Loading from "../common/Loading";

import "./Auth.css";

class Register extends Component {
  constructor() {
    super();

    this.state = {
      errors: {},
      success: false,
      name: "",
      email: "",
      password: "",
      repassword: ""
    };

    this.fields = {
      name: {
        label: "Ваше имя",
        icon: "fa fa-user fa-fw"
      },
      email: {
        label: "Эл. почта",
        icon: "fa fa-envelope fa-fw"
      },
      password: {
        label: "Пароль",
        icon: "fa fa-lock fa-fw"
      },
      repassword: {
        label: "Повтор пароля",
        icon: "fa fa-lock fa-fw"
      }
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  handleSubmit(e) {
    e.preventDefault();

    const { errors, success, ...newUser } = this.state;

    if (newUser.password !== newUser.repassword) {
      return this.setState({
        errors: { repassword: "пароли должны совпадать" }
      });
    }

    // this.props.registerUser(newUser);
    console.log("new user: ", newUser);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.registration && nextProps.auth.registration.success) {
      this.setState({
        errors: {},
        success: false,
        name: "",
        email: "",
        password: "",
        repassword: ""
      });
    }

    if (nextProps.auth.errors) {
      this.setState({
        errors: nextProps.auth.errors,
        success: {}
      });
    }
  }

  render() {
    const { errors } = this.state;
    let submitButton;

    const {
      auth: { registration, ...auth }
    } = this.props;
    let alert = {};

    if (registration && registration.success) {
      alert.type = "primary";
      alert.text = (
        <p className="m-0">
          <small>
            <i className="glyphicon glyphicon-thumbs-up" />{" "}
            {registration.message}
          </small>
        </p>
      );
    } else {
      alert.type = "info";
      alert.text = (
        <p className="m-0">
          <small>Заполните все поля формы</small>
        </p>
      );
    }

    if (!auth.loading) {
      submitButton = (
        <button type="submit" className="btn btn-info btn-block ">
          <span className="fa fa-send fa-fw" /> Зарегистрироваться
        </button>
      );
    } else {
      submitButton = (
        <button
          type="submit"
          disabled
          className="btn btn-info btn-block disabled"
        >
          <Loading />
        </button>
      );
    }

    return (
      <div className="page page_register">
        <div className="register-form">
          <div className="container">
            <div className="form-layout border shadow-sm">
              <div className="form-addon">
                <div className="addon__title">
                  <h2 className="form-title">
                    <i class="fas fa-pencil-alt fa-fw mr-1" />
                    Форма регистрации
                  </h2>
                </div>
                <div className="addon__content">
                  <p className="addon__text">
                    добро пожаловать в базу специалистов группы компаний
                    Фасадкрым! Если хотите получать предложения по работе от
                    нас, просто заполните небольшую анкету.
                  </p>
                  <Link
                    to="/application"
                    className="btn btn-fk btn-outline-light mx-auto btn-app-form"
                  >
                    <i className="fa fa-address-card fa-fw mr-2" /> заполнить
                    анкету
                  </Link>
                  <p className="addon__text mt-4">
                    если хотите в дальнейшем редактировать анкету сами, то
                    необходима регистрация через нашу форму
                  </p>
                  {/* <span className="text-devider">или</span> */}
                </div>
              </div>
              <form
                noValidate
                onSubmit={this.handleSubmit}
                className="p-5 auth-form form-horizontal bg-muted"
                id="contact_form"
              >
                <fieldset className="fieldset">
                  {/* <!-- Form Name --> */}
                  {/* <legend className="border-bottom">
                                    <center>
                                        <h2 className="form-title">
                                            <b>Форма регистрации</b>
                                        </h2>
                                    </center>
                                </legend>
                                <br /> */}

                  {/* <Alert type={alert.type} text={alert.text} classes="text-center" />

                                <Link 
                                    className="btn btn-lg btn-primary btn-block mb-4 btn-fk"
                                    to='/application'
                                >
                                    <i className="fa fa-address-card fa-fw mr-2"></i>
                                    <small>хочу просто отправить анкету!</small>
                                </Link> */}

                  {_.map(this.fields, (field, name) => {
                    const type = /password/.test(name) ? "password" : "text";

                    return (
                      <FormFieldFactory
                        key={name}
                        name={name}
                        icon={field.icon}
                        value={this.state[name]}
                        placeholder={field.label}
                        handleChange={this.handleChange}
                        type="input"
                        inputType={type}
                        error={errors[name]}
                      />
                    );
                  })}

                  <div className="form-group">
                    <label className="col-md-4 control-label" />
                    <div className="col-xs-12">
                      {submitButton}
                      <small className="form-text text-muted text-center">
                        Уже есть аккаунт?{" "}
                        <Link className="font-weight-bold" to="/login">
                          Войти
                        </Link>
                      </small>
                    </div>
                  </div>
                </fieldset>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Register.propTypes = {
  registerUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  // { registerUser }
  {}
)(Register);
