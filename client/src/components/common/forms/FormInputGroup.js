import React from 'react';
import classnames from "classnames";
import PropTypes from 'prop-types';

const FormInputGroup = ({
  icon,
  label,
  disabled,
  inputType,
  value,
  name,
  placeholder,
  error,
  handleChange,
  info
}) => {
  return (
    <div className="form-group">
      <div className="col-xs-12 inputGroupContainer">
        {/* {label && <label className="control-label">{label}</label>}
        {info && <small className="form-text text-muted">{info}</small>} */}
        <div className="control__header">
          {label && (
            <label className="control__title pull-left">
              {label}
            </label>
          )}  
          {info && <p className="control__subtitle">{info}</p>}
        </div>

        <div className="input-group">
          <div className="input-group-prepend">
            <span className="input-group-text"><i className={icon}></i></span>
          </div>
          <input
            value={value}
            name={name}
            placeholder={placeholder}
            disabled={disabled}
            className={classnames("form-control", {
              "is-invalid": error
            })}
            onChange={handleChange.bind(this)}
            type={inputType}
          />          
          {error && <span className="invalid-feedback">{error}</span>} 
        </div>
               
      </div>
    </div>
  )
}

FormInputGroup.propTypes = {
  disabled: PropTypes.bool.isRequired,
  label: PropTypes.string,
  inputType: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  error: PropTypes.string,
  handleChange: PropTypes.func.isRequired,
  info: PropTypes.string
}

FormInputGroup.defaultProps = {
  value: '',
  inputType: 'text',
  disabled: false
}

export default FormInputGroup;