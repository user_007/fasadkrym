import React from "react";
import PropTypes from "prop-types";
import _ from 'lodash';

import ImageGallery from 'react-image-gallery';

import Loading from "../../common/Loading";

import "react-image-gallery/styles/css/image-gallery.css";
import './FormFields.css';

const FormMultiFile = ({
  id, label, name, files, error, disabled, info, handleChange
}) => {
  const filesInput = React.createRef();
  const carousel = React.createRef();

  const removeFile = e => {
    e.preventDefault();
    const slider = carousel.current;
    const index = slider.getCurrentIndex();
    let newIndex;

    const newFilesList = files.filter(file => file !== files[index]);

    switch (index) {
      case 0:
        newIndex = index + 1;
        break;
      case files.length - 1:
        newIndex = index - 1;
        break;
      default:
        newIndex = index;
    }

    slider.slideToIndex(newIndex);

    handleChange(newFilesList);
  }

  const handleUploadPhoto = event => {
    event.preventDefault();

    filesInput.current.click();
  };

  const formFileObject = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    let fileObject = {
      file,
      preview: null
    };

    reader.readAsDataURL(file);

    reader.onload = e => {
      fileObject.preview = e.target.result;
    }

    reader.onloadend = e => {
      resolve(fileObject)
    }
  });

  const getListOfFiles = async (e) => {
    e.preventDefault();

    const { files } = filesInput.current;

    try {
      disabled = true;
      let listOfFiles = await Promise.all(
        _.map(files, (file, index) => {
          console.log('File + index: ', file, index);

          return formFileObject(file);
        })
      )

      handleChange(listOfFiles);
    } catch (ex) {
      console.log('Error occured during reading files: ', ex)
    } finally {
      disabled = false;
    }




  }

  const _renderItem = item => {
    const removeSlideButton = disabled ? (
      <button
        className="file-remove-btn btn btn-info btn-sm disabled"
        onClick={undefined}
        disabled={disabled}
      >
        <Loading />
      </button>
    ) : (
        <button
          className="file-remove-btn btn btn-info btn-sm"
          onClick={removeFile}
        >
          <i className="fa fa-trash"></i> удалить
      </button>
      )

    return (
      <div className='multi-file-container'>
        <div className="image-gallery-image">
          <img
            className="mh-100 mw-100"
            src={item.original}
            alt={item.originalAlt}
            srcSet={item.srcSet}
            sizes={item.sizes}
            index={item.index}
          />
          {removeSlideButton}
          {
            item.description &&
            <span className='image-gallery-description'>
              {item.description}
            </span>
          }
        </div>
      </div>
    )
  }

  // let filesList = '';
  // filesList += files.length > 0 ? files.map(({ file }) => ' ' + file.name) : 'Список файлов пуст...'

  const sliderContent = files.length > 0
    ? <ImageGallery
      ref={carousel}
      items={
        files.map((file, index) => {
          return {
            original: file.preview,
            thumbnail: file.preview,
            index
          }
        })
      }
      showPlayButton={false}
      showFullscreenButton={false}
      thumbnailPosition="bottom"
      renderItem={_renderItem}
      showIndex={true}
    /> : (
      <div className="multi-file-container nofiles">
        <img
          src="https://res.cloudinary.com/fk29intranet/image/upload/v1542787996/no-image.png"
          alt="файлов нет"
          className="profile-photo-preview"
        />
      </div>
    );

  const uploadBtn = disabled ? (
    <button
      disabled={disabled}
      onClick={undefined}
      className="btn btn-info btn-block btn-sm disabled mt-2"
    >
      <Loading />
    </button>

  ) : (
      <button
        disabled={disabled}
        className="btn btn-info btn-block btn-sm mt-2"
        onClick={handleUploadPhoto}
      >
        <i className="fa fa-image fa-fw" /> Загрузить фото
    </button>
    )

  return (
    <div className="container">
        <div className="control__header">
          {label && <h4 className="control__title">{label}</h4>}
          {info && <p className="control__subtitle">{info}</p>}
        </div>
        {sliderContent}
        <div className="multi-file-controls">


          <input
            id={id}
            disabled={disabled}
            type="file"
            ref={filesInput}
            multiple={true}
            className={`js-load-photo hidden${disabled ? ' disabled' : ''}`}
            name={name}
            onChange={disabled ? undefined : getListOfFiles}
            files={files}
          />

          {/* <div className={`alert ${error ? 'alert-danger' : 'alert-info'} multi-file__alert`}>
            <p className="m-0">{error ? error : filesList.trim()}</p>
          </div> */}

          {
            files.length === 0 &&
            <div className="alert alert-info multi-file__alert">
              <p className="m-0">Список фалов пуст...</p>
            </div>
          }

          {uploadBtn}
        </div>
      <div className="clearfix"></div>
    </div>
  );
};

FormMultiFile.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  id: PropTypes.string,
  files: PropTypes.array.isRequired,
  disabled: PropTypes.bool,
  info: PropTypes.string,
  error: PropTypes.string,
  handleChange: PropTypes.func.isRequired
};

FormMultiFile.defaultProps = {
  disabled: false,
  files: []
};

export default FormMultiFile;
