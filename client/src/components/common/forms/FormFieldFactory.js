import React from "react";
import PropTypes from "prop-types";

import FormInput from "./FormInput";
import FormSelect from "./FormSelect";
import FormInputGroup from "./FormInputGroup";
import FormTextArea from "./FormTextArea";
import FormFile from "./FormFile";
import FormMultiFile from "./FormMultiFile";
import FormDatePicker from "./FormDatePicker";

import "./FormFields.css";

/**
 *
 * @param {String} param5 type of form field
 * [textarea: render <textarea></teaxtarea>]
 */
const FormFieldFactory = ({
  icon,
  options,
  label,
  preview,
  disabled,
  type,
  inputType,
  value,
  name,
  placeholder,
  error,
  handleChange,
  handleRemoveFile,
  info,
  id,
  files,
  rows,
  loading,
  mask
}) => {
  const commonProps = {
    label,
    disabled,
    name,
    error,
    handleChange,
    info
  };

  switch (type) {
    case "select":
      return (
        <FormSelect
          {...commonProps}
          options={options}
          loading={loading}
          value={value}
          icon={icon}
        />
      );
    case "textarea":
      return (
        <FormTextArea
          {...commonProps}
          rows={rows}
          value={value}
          placeholder={placeholder}
        />
      );
    case "input-group":
      return (
        <FormInputGroup
          {...commonProps}
          icon={icon}
          value={value}
          placeholder={placeholder}
          inputType={inputType}
        />
      );
    case "input":
      return (
        <FormInput
          {...commonProps}
          value={value}
          placeholder={placeholder}
          inputType={inputType}
          mask={mask}
          icon={icon}
        />
      );
      case "date":
      return (
        <FormDatePicker 
          {...commonProps}
          value={value}
          onChange={handleChange}
          placeholder={placeholder}
          icon={icon}
        />
      )
    case "file":
      return <FormFile {...commonProps} preview={preview} />;
    case "multi-file":
      return (
        <FormMultiFile
          {...commonProps}
          files={files}          
          preview={preview}
          id={id}
        />
      );
    default:
      //todo: подумать, что выводить по дефолту
      return null;
  }
};

FormFieldFactory.propTypes = {
  id: PropTypes.string,
  preview: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.array
  ]),
  options: PropTypes.array,
  loading: PropTypes.bool,
  icon: PropTypes.string,
  type: PropTypes.string.isRequired,
  label: PropTypes.string,
  value: PropTypes.string,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  error: PropTypes.string,
  files: PropTypes.array,
  handleRemoveFile: PropTypes.func,
  handleChange: PropTypes.func.isRequired,
  info: PropTypes.string,
  mask: PropTypes.object
};

FormFieldFactory.defaultProps = {
  type: "text",
  disabled: false
};

export default FormFieldFactory;
