import React from "react";
import PropTypes from "prop-types";

const FormFile = ({name, preview, error, disabled, info, handleChange}) => {
  const fileInput = React.createRef();

  const handleUploadPhoto = event => {
    event.preventDefault();

    fileInput.current.click();
  };


  const onFileChange = event => {
    // event.preventDefault();
    const { files } = fileInput.current;

    if (files && files[0]) {
      const reader = new FileReader();

      reader.onload = e => {
        handleChange({ file: files[0], preview: e.target.result });
      };

      reader.readAsDataURL(files[0]);
    }
  };

  return (
    <div className="form-group">
      <input
        disabled={disabled}
        type="file"
        ref={fileInput}
        className="js-load-photo hidden"
        name={name}
        onChange={onFileChange}
      />
      <div className="file-preview-container">
        <img
          src={preview}
          alt="фотография пользователя"
          className="profile-photo-preview"
        />
      </div>
      <button
        disabled={disabled}
        className="btn btn-info btn-block btn-sm mt-4 mb-2"
        onClick={handleUploadPhoto}
      >
        <i className="fa fa-image fa-fw" /> Загрузить фото
      </button>
      {(info && !error) && <p className="control__subtitle text-center">{info}</p>}
      {error && <small>{error}</small>}
    </div>
  );
};

FormFile.propTypes = {
  name: PropTypes.string.isRequired,
  preview: PropTypes.string,
  disabled: PropTypes.bool,
  info: PropTypes.string,
  error: PropTypes.string,
  handleChange: PropTypes.func.isRequired
};

FormFile.defaultProps = {
  preview : "https://res.cloudinary.com/im-projects/image/upload/w_200,h_200,c_thumb,g_face,r_max,e_sharpen/v1535285821/qd/no-image.png",
  disabled: false
};

export default FormFile;
