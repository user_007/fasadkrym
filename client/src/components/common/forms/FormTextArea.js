import React from "react";
import classnames from "classnames";
import PropTypes from "prop-types";

const FormTextArea = ({
  label,
  disabled,
  value,
  name,
  placeholder,
  error,
  handleChange,
  info,
  rows
}) => {
  return (
    <div className="form-group">
      <div className="col-xs-12">
        <div className="control__header">
          {label && 
            <label className="control__title">
              {label}
            </label>
          }
          {info && <p className="control__subtitle">{info}</p>}
        </div>
        <textarea
          rows={rows}
          name={name}
          placeholder={placeholder}
          disabled={disabled}
          className={classnames("form-control", {
            "is-invalid": error
          })}
          onChange={handleChange.bind(this)}
          value={value}
        >
          
        </textarea>
        {error && <span className="invalid-feedback">{error}</span>}
      </div>
    </div>
  );
};

FormTextArea.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  error: PropTypes.string,
  handleChange: PropTypes.func.isRequired,
  info: PropTypes.string
};

FormTextArea.defaultProps = {
  value: "",
  disabled: false
};

export default FormTextArea;
