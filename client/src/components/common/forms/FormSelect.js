import React from "react";
import classnames from "classnames";
import PropTypes from "prop-types";
import Loading from "../Loading";
import { v4 as idGen } from "uuid";

const FormSelect = ({
  label,
  disabled,
  options,
  name,
  error,
  handleChange,
  info,
  loading,
  icon,
  value
}) => {
  let list = [];
  const withIcon = icon && icon.length;

  if (loading) {
    list = (
      <option className="disabled" disabled={true}>
        {label}
      </option>
    );
  } else {
    list = options.map(option => {
      return (
        <option key={idGen()} value={option.value}>
          {option.text}
        </option>
      );
    });
  }

  return (
    <div className="form-group">
      <div className="col-xs-12">
        <div className="control__header">
          {label && (
            <label className="control__title pull-left">
              {label} {loading && <Loading />}
            </label>
          )}  
          {info && <p className="control__subtitle">{info}</p>}
        </div>
        
        <div className="field">
          <select
            name={name}
            disabled={disabled || loading}
            className={classnames("form-control", {
              "is-invalid": error,
              "field-with-icon": withIcon
            })}
            value={value}
            onChange={handleChange.bind(this)}
          >
            {/* {options.map(option => {
              return <option key={option.value} value={option.value}>{option.text}</option>;
            })} */}
            {list}
          </select>
          {error && <span className="invalid-feedback">{error}</span>}
          {withIcon && <i className={`fa ${icon} fa-fw field__icon`}></i>}  
        </div>
      </div>
    </div>
  );
};

FormSelect.propTypes = {
  options: PropTypes.array.isRequired,
  label: PropTypes.string,
  loading: PropTypes.bool,
  name: PropTypes.string.isRequired,
  disabled: PropTypes.bool.isRequired,
  error: PropTypes.string,
  handleChange: PropTypes.func.isRequired,
  info: PropTypes.string,
  icon: PropTypes.string
};

FormSelect.defaultProps = {
  disabled: false
};

export default FormSelect;
