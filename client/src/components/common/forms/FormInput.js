import React from 'react';
import classnames from "classnames";
import PropTypes from 'prop-types';

const FormInput = ({
  label,
  disabled,
  inputType,
  value,
  name,
  placeholder,
  error,
  handleChange,
  mask,
  info,
  icon
}) => {  

  const additionalProps = mask !== undefined ? mask : {}

  const withIcon = icon && icon.length;

  return (
    <div className="form-group">
      <div className="col-xs-12">
        <div className="control__header">
          {label && <label className="control__title">{label}</label>}
          {info && <p className="control__subtitle">{info}</p>}
        </div>
        <div className="field">
          <input
            value={value}
            name={name}
            placeholder={placeholder}
            disabled={disabled}
            className={classnames("form-control", {
              "is-invalid": error,
              "field-with-icon": withIcon
            })}
            onChange={handleChange.bind(this)}
            type={inputType}
            {...additionalProps}
          />        
          {error && <span className="invalid-feedback">{error}</span>}    
          {withIcon && <i className={`fa ${icon} fa-fw field__icon`}></i>}  
        </div>         
      </div>
    </div>
  )
}

FormInput.propTypes = {
  inputType: PropTypes.string.isRequired,
  label: PropTypes.string,
  value: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  error: PropTypes.string,
  handleChange: PropTypes.func.isRequired,
  info: PropTypes.string,
  mask: PropTypes.object,
  icon: PropTypes.string
}

FormInput.defaultProps = {
  value: '',
  inputType: 'text',
  disabled: false
}

export default FormInput;