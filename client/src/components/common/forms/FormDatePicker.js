import React from 'react';
import PropTypes from 'prop-types';
import DatePicker from 'react-datepicker';
import classnames from 'classnames';
import moment from 'moment';

import "react-datepicker/dist/react-datepicker.css";

function FormDatePicker({
  label, name, info, icon, placeholder, error, handleChange, value, disabled
}) {
  const val = value.length ? moment(value) : null;
  const withIcon = icon && icon.length;

  return (
    <div className="form-group">
      <div className="col-xs-12">
        <div className="control__header">
          {label && <label className="control__title">{label}</label>}
          {info && <p className="control__subtitle">{info}</p>}
        </div>
        <div className="field">
          <DatePicker
            selected={val}
            className={classnames("form-control", {
              "field-with-icon": withIcon
            })}
            onChange={handleChange}
            placeholderText={placeholder}
            name={name}
            dateFormat="DD.MM.YYYY"
            disabled={disabled}
          />
          {error && <span className="invalid-feedback">{error}</span>}
          {withIcon && <i className={`fa ${icon} fa-fw field__icon`}></i>}  
        </div>
      </div>
    </div>

  )
}

FormDatePicker.propTypes = {
  label: PropTypes.string, 
  info: PropTypes.string, 
  placeholder: PropTypes.string, 
  error: PropTypes.string, 
  handleChange: PropTypes.func, 
  value: PropTypes.string, 
  disabled: PropTypes.bool
}

export default FormDatePicker

