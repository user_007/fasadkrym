import React from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";

const PrivateRoute = ({
  redirectPath,
  auth,
  ...otherProps
}) => {

  return auth.isAuthenticated ? 
  (<Route {...otherProps} />) :
  (<Redirect to={redirectPath} />)
};

PrivateRoute.propTypes = {
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps)(PrivateRoute);
