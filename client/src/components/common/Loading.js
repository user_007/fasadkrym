import React from 'react'

const Loading = () => {
  return (
    <span className="loading">
      <i className="fa fa-spinner fa-spin"></i> Загрузка...
    </span>
  )
}

export default Loading;
