import React from "react";
import PropTypes from "prop-types";

const Alert = ({
  text,
  type,
  classes
}) => {
  const classname = `alert alert-${type} mb-4 ${classes}`;

  return (
    <div className={classname} role="alert">{text}</div>
  );
};

Alert.propTypes = {
  text: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  classes: PropTypes.string
}

export default Alert;
