import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { formatPhone } from '../../../utils'

import './UserCard.css';

function UserCard({
  user
}) {
  return (
    <div className="employee_card card shadow-sm">

      <div className="card__section employee-photo_section">

        <img 
          src={user.photo} 
          alt={`${user.lastName} ${user.firstName}`} 
          className="mh-100 rounded-circle" 
        />

      </div>


      <div className="card__section">

        <Link
          to={`/users/${user._id}`}
          className="employee__name"
        >
          <b>{`${user.lastName} ${user.firstName}`}</b>
        </Link>

        <small className="employee__spec">
          <i className="fa fa-fw fa-tag"></i> Специализация: {user.specialization}
        </small>

      </div>


      <div className="card__section phone_section">

        <p className="employee__phone">{ formatPhone(user.phone) }</p>

      </div>


      <div className="card__section">

        <a 
          href={`/users/${user._id}`} 
          className="btn btn-xs btn-outline-secondary btn-block"
        >

          <i className="fa fa-id-card fa-fw"></i> перейти в профиль

        </a>

      </div>


      {/* <div className="card__section">
        <p className="employee__about">
          {user.info}
                <button className="btn-link btn description-link">подробнее</button>
        </p>
      </div> */}
      
    </div>
  )
}

UserCard.propTypes = {
  user: PropTypes.object.isRequired
}

UserCard.defaultProps = {
  user: {}
}

export default UserCard

