import React from 'react';
import PropTypes from 'prop-types';
import ImageGallery from 'react-image-gallery';
import { formatDate } from '../../../utils';

import './ObjectCard.css';

function ObjectCard({ objectData }) {
  const { title, photo, category, details: { dateFinish: date }, location: { address } } = objectData;
  const content = photo.length ? photo.map(img => {
    return {
      original: img
    }
  }) : [{
    original: ['https://res.cloudinary.com/fk29intranet/image/upload/v1542787996/no-image.png']
  }]

  const _renderItem = item => {

    return (
        <div className="image-gallery-image object__photo-wrapper">
          <img
            className="object__photo"
            src={item.original}
            alt={item.originalAlt}
            srcSet={item.srcSet}
            sizes={item.sizes}
            index={item.index}
          />
          {/* {
            item.description &&
            <span className='image-gallery-description'>
              {item.description}
            </span>
          } */}
        </div>
    )
  }

  return (
      <div className="card object_card shadow-sm">

        <ImageGallery 
          showThumbnails={false} 
          items={content} 
          alt={address}
          className="object__photo"
          showPlayButton={false}
          renderItem={_renderItem}
          showFullscreenButton={photo.length > 0}
        />


      <ul className="object__data-list">

        <li className="object__data-item object_name">{ title }</li>

        <li className="object__data-item object_category">
          <i className="fa fa-tag fa-fw"></i> { category }</li>

        <li className="object__data-item object_address">
          <i className="fa fa-map-marker-alt fa-fw"></i> { address }</li>

        <li className="object__data-item object_date">
          <i className="fa fa-calendar-alt fa-fw"></i> { formatDate(date) }</li>

      </ul>

    </div>
  )
}

ObjectCard.propTypes = {
  objectData: PropTypes.object.isRequired
}

// ObjectCard.defaultProps = {
//   images: ['https://res.cloudinary.com/fk29intranet/image/upload/v1542787996/no-image.png']
// }

export default ObjectCard;

