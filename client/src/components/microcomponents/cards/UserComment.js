import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import './UserComment.css';

function UserComment({
  text, status, published
}) {
  return (
    <div className={`alert comment comment_${status}`}>

      <small className="comment__published">{moment(published).format('HH:mm DD.MM.YY')}</small>
      <p className="comment__text">{text}</p>

    </div>
  )
}

UserComment.propTypes = {
  text: PropTypes.string,
  status: PropTypes.string,
  published: PropTypes.object
}

export default UserComment;
