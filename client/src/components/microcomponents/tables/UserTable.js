import React from 'react';
import PropTypes from 'prop-types';
import keyGen from 'uuid/v4';

function UserTable({userList}) {
  //Table row
  function Row({user}) {
    return (
      <tr>
        <td></td>
      </tr>
    )
  }

  const tbody = userList.map(user => <Row key={keyGen()} user={user} />)

  return (
    <table className="user-table">
      <tbody>
        {tbody}
      </tbody>
    </table>
  )
}

UserTable.propTypes = {
  userList: PropTypes.array.isRequired
}

UserTable.defaultProps = {
  userList: []
}

export default UserTable

