import React from 'react'
import PropTypes from 'prop-types'

function ModalDocumentForm({ handleApplyDocument }) {
  const formRef = React.createRef();

  const resetErrors = e => {
    const input = e.currentTarget;

    if (input.style.borderColor === "red") input.style.borderColor = "initial";
  }

  const applyDocument = () => {
    let document = {};
    let errors = [];

    const form = formRef.current;

    for ( const input of form.elements ) {
      const {name, value} = input;

      value.length ? document[name] = value : errors.push(name)
    }
    
    console.log('form data: ', document);
    
    if (errors.length) {
      console.log('Необходимо заполнить следующие поля: ', errors);

      errors.forEach(name => form.elements.namedItem(name).style.border = "1px solid red")
    } else {
      handleApplyDocument(document);
      form.clear();
    }
  }


  return (
    <div className="document-form">
      
      <form ref={formRef} id="test">
        <div className="form-group">
          <label htmlFor="name">Название</label>
          <input 
            type="text" name="name" 
            onFocus={resetErrors}
            className="form-control input-sm" 
          />
        </div>
        <div className="form-group">
          <label htmlFor="number">Номер</label>
          <input 
            type="text" name="number" 
            onFocus={resetErrors}
            className="form-control input-sm" 
          />
        </div>
        <div className="form-group">
          <label htmlFor="issuedBy">Кем выдан:</label>
          <input 
            type="text" name="issuedBy" 
            onFocus={resetErrors}
            className="form-control input-sm" 
          />
        </div>
        <div className="form-group">
          <label htmlFor="issueDate">Дата выдачи:</label>
          <input 
            type="text" name="issueDate" 
            onFocus={resetErrors}
            className="form-control input-sm" 
          />
        </div>
      </form>

      <button onClick={applyDocument}>Проверить</button>
    </div>
  )
}

ModalDocumentForm.propTypes = {
  handleApplyDocument: PropTypes.func.isRequired
}

export default ModalDocumentForm;
