import moment from 'moment';

export const formatPhone = phone => {
  return phone.replace(/(\d{1})(\d{3})(\d{3})(\d{2})(\d{2})/, '+$1\xa0($2)\xa0$3-$4-$5');
}

export const formatDate = date => {
  return moment(date).format('DD.MM.YYYY');
}

export const serialize = obj => {
  var str = [];
  for (let prop in obj)
    if (obj.hasOwnProperty(prop) && obj[prop] !== null) {
      str.push(encodeURIComponent(prop) + "=" + encodeURIComponent(obj[prop]));
    }
  return str.join("&");
}
