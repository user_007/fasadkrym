import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';

import './App.css';

import store from './store';

import Navbar from './components/layout/Navbar';
import Footer from './components/layout/Footer';
import MainpageComponent from './components/Mainpage';
import RegisterPage from './components/authentication/Register';
import UsersPage from './components/pages/UsersPage';
import UserDataPage from './components/pages/UserDataPage';
import ApplicationFormPage from './components/pages/ApplicationForm';
import ObjectPage from './components/pages/ObjectPage';
import NewObjectFormPage from './components/pages/NewObjectFormPage';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div className="App">

            <Switch>              
              <Route path='/admin' component={null /* TODO: Make admin navbar component */} />
              <Route path='/' component={Navbar} />            
            </Switch>
            <Route exact path='/register' component={RegisterPage} />
            <Route exact path='/users' component={UsersPage} />
            <Route exact path='/users/:userId' component={UserDataPage} />
            <Route exact path='/objects' component={ObjectPage} />
            <Route exact path='/objects/add' component={NewObjectFormPage} />
            <Route exact path='/objects/:id/edit' component={NewObjectFormPage} />
            <Route exact path='/application' component={ApplicationFormPage} />
            <Route exact path='/' component={MainpageComponent} />
            <Route path='/' component={Footer} />
          </div>
        </Router>

      </Provider>
    );
  }
}

export default App;
