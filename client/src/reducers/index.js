import { combineReducers } from 'redux';

import authReducers from './authReducers';
import userReducers from './userReducers';
import objectReducers from './objectReducers';

export default combineReducers({
  auth: authReducers,
  users: userReducers,
  objects: objectReducers
})