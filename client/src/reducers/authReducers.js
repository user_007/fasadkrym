const initialState = {
  errors: null,
  loading: false,
  accountData: null,
  isAuthorised: false
}

export default (state = initialState, action) => {
  switch (action) {
    default:
      return {
        ...state
      }
  }
}