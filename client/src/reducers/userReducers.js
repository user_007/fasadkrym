import {
  ADD_NEW_USER, RESET_NEW_USER, GET_USER_LIST, GET_USER_DATA, RESET_USER_DATA, ADD_USER_COMMENT
} from '../actions/types'

const initialState = {
  errors: null,
  loading: false,
  loadingNewComment: false,
  newComment: null,
  list: [],
  currentUserData: null,
  newUser: null
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_NEW_USER.START:
      return {
        ...state,
        loading: true
      };
    case ADD_NEW_USER.SUCCESS:
      return {
        ...state,
        ...payload,
        loading: false
      };
    case ADD_NEW_USER.FAILURE:
      return {
        ...state,
        ...payload,
        loading: false
      };
    case RESET_NEW_USER:
      return {
        ...state,
        newUser: null
      };
    case RESET_USER_DATA:
      return {
        ...state,
        currentUserData: null,
        newUser: null
      };
    case GET_USER_LIST.START:
      return {
        ...state,
        loading: true
      };
    case GET_USER_LIST.SUCCESS:
      return {
        ...state,
        ...payload,
        loading: false
      };
    case GET_USER_LIST.FAILURE:
      return {
        ...state,
        ...payload,
        loading: false
      };
    case GET_USER_DATA.START:
      return {
        ...state,
        newComment: null,
        errors: null,
        loading: true
      };
    case GET_USER_DATA.SUCCESS:
      return {
        ...state,
        ...payload,
        loading: false
      };
    case GET_USER_DATA.FAILURE:
      return {
        ...state,
        ...payload,
        loading: false
      };
    case ADD_USER_COMMENT.START:
      return {
        ...state,
        newComment: null,
        loadingNewComment: true
      };
    case ADD_USER_COMMENT.SUCCESS:
      return {
        ...state,
        newComment: payload.comment,
        loadingNewComment: false
      };
    case ADD_USER_COMMENT.FAILURE:
      return {
        ...state,
        loadingNewComment: false,
        errors: payload.errors
      }
    default:
      return {
        ...state
      }
  }
}