import {
  SAVE_NEW_OBJECT_START,
  SAVE_NEW_OBJECT_SUCCESS,
  SAVE_NEW_OBJECT_FAILURE,
  GET_OBJECT_CATEGORIES_START,
  GET_OBJECT_CATEGORIES_SUCCESS,
  GET_OBJECT_CATEGORIES_FAILURE,
  GET_OBJECTS
} from '../actions/types'

const initialState = {
  errors: null,
  list: [],
  categories: [],
  loading: false,
  new: null
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_OBJECT_CATEGORIES_START:
      return {
        ...state,
        loading: true
      }
    case GET_OBJECT_CATEGORIES_SUCCESS:
      return {
        ...state,
        loading: false,
        categories: payload.list
      }
    case GET_OBJECT_CATEGORIES_FAILURE:
      return {
        ...state,
        loading: false,
        ...payload
      }
    case SAVE_NEW_OBJECT_START:
      return {
        ...state,
        loading: true
      }
    case SAVE_NEW_OBJECT_SUCCESS:
      return {
        ...state,
        loading: false,
        ...payload
      }
    case SAVE_NEW_OBJECT_FAILURE:
      return {
        ...state,
        loading: false,
        ...payload
      };
    case GET_OBJECTS.START:
      return {
        ...state,
        loading: true
      };
    case GET_OBJECTS.SUCCESS:
      return {
        ...state,
        loading: false,
        ...payload
      };
    case GET_OBJECTS.FAILURE:
      return {
        ...state,
        loading: false,
        ...payload
      };
    default:
      return {
        ...state
      }
  }
}