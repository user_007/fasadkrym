import axios from 'axios';

import {
  GET_OBJECT_CATEGORIES_SUCCESS, GET_OBJECT_CATEGORIES_START, GET_OBJECT_CATEGORIES_FAILURE, SAVE_NEW_OBJECT_FAILURE, SAVE_NEW_OBJECT_START, SAVE_NEW_OBJECT_SUCCESS, GET_OBJECTS
} from './types';

import { serialize } from '../utils'


export const getCategoriesList = () => dispatch => {
  dispatch({
    type: GET_OBJECT_CATEGORIES_START
  });

  axios
    .get('/api/work/cat-list')
    .then(res => {
      dispatch({
        type: GET_OBJECT_CATEGORIES_SUCCESS,
        payload: {
          ...res.data
        }
      })
    })
    .catch(ex => {
      dispatch({
        type: GET_OBJECT_CATEGORIES_FAILURE,
        payload: ex.response.data
      })
    })
}

export const getListOfObjects = filters => dispatch => {
  dispatch({
    type: GET_OBJECTS.START
  })

  //TODO: filters
  console.log('Objects list filters: ', filters);
  const query = serialize(filters);

  console.log('Serialized query: ', query)
  console.log('S query type: ', typeof query)

  axios
    .get(`/api/work?${query}`)
    .then(res => {
      console.log('DATA GOT FROM OBJ ACTIONS: ', res.data);
      dispatch({
        type: GET_OBJECTS.SUCCESS,
        payload: { ...res.data }
      })
    })
    .catch(ex => {
      dispatch({
        type: GET_OBJECTS.FAILURE,
        payload: { ...ex.response.data }
      })
    })
}

export const saveNewObject = objectData => dispatch => {
  dispatch({
    type: SAVE_NEW_OBJECT_START
  });

  axios
    .post('/api/work/add', objectData)
    .then(res => {
      dispatch({
        type: SAVE_NEW_OBJECT_SUCCESS,
        payload: {
          ...res.data
        }
      })
    })
    .catch(ex => {
      dispatch({
        type: SAVE_NEW_OBJECT_FAILURE,
        payload: ex.response.data
      })
    });
}
