import axios from 'axios';

import { 
  ADD_NEW_USER, RESET_NEW_USER, GET_USER_LIST, GET_USER_DATA, RESET_USER_DATA, ADD_USER_COMMENT 
} from './types';

export const addNewUser = userData => dispatch => {
  dispatch({
    type: ADD_NEW_USER.START
  })

  axios
    .post('/api/users/admin/user-add', userData)
    .then(res => {
      dispatch({
        type: ADD_NEW_USER.SUCCESS,
        payload: res.data
      })
    })
    .catch(ex => {
      dispatch({
        type: ADD_NEW_USER.FAILURE,
        payload: ex.response.data
      })
    })
}

export const getUserList = () => dispatch => {
  dispatch({
    type: GET_USER_LIST.START
  });

  axios
    .get('/api/users')
    .then(res => {
      dispatch({
        type: GET_USER_LIST.SUCCESS,
        payload: res.data
      })
    })
    .catch(ex => {
      dispatch({
        type: GET_USER_LIST.FAILURE,
        payload: ex.response.data
      })
    })
}

export const getUserByID = id => dispatch => {
  dispatch({
    type: GET_USER_DATA.START
  });

  axios
    .get(`/api/users/${id}`)
    .then(res => dispatch({
      type: GET_USER_DATA.SUCCESS,
      payload: res.data
    }))
    .catch(ex => dispatch({
      type: GET_USER_DATA.FAILURE,
      payload: ex.response.data
    }))
}

export const resetNewUser =() => dispatch => {
  dispatch({
    type: RESET_NEW_USER
  });
}

export const resetUserData = () => dispatch => {
  dispatch({
    type: RESET_USER_DATA
  })
}

export const addUserComment = (userId, comments) => dispatch => {
  const { START, SUCCESS, FAILURE } = ADD_USER_COMMENT;

  dispatch({
    type: START
  });

  console.log('Comments data from action: ', comments);

  axios
    .post(`/api/users/${userId}/add-comment`, comments)
    .then(res => {
      dispatch({
        type: SUCCESS,
        payload: res.data
      })
    })
    .catch(ex => {
      dispatch({
        type: FAILURE,
        payload: ex.response.data
      })
    })
}
