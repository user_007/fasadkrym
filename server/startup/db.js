const mongoose = require('mongoose');
const settings = require('../config/settings')

module.exports.dbConnect = () => mongoose.connect(
  settings.db, 
  { 
    useNewUrlParser: true, 
    useCreateIndex: true 
  }
);
