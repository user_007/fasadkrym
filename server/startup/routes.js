const homeRoute = require('../routes/homepage');
const userRoute = require('../routes/userRoute');
const workRoute = require('../routes/workRoute');

module.exports = app => {
    app.use('/api', homeRoute);
    app.use('/api/users', userRoute);
    app.use('/api/work', workRoute);
}
