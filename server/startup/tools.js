const bodyparser = require("body-parser");

module.exports = app => {
  app.use(bodyparser.json({ limit: '50mb', extended: true }));
  app.use(bodyparser.urlencoded({ limit: '50mb', extended: true }));
};
