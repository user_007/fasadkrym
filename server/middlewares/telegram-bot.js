const TelegramBot = require('node-telegram-bot-api');
const { telegram: { token, fk1 } } = require('../config/settings');

const _BotCommand = {

  SEND_INFO: '/get_info',
  CREATE_OFFER: '/create_offer',
  GREETINGS: '/greet_me',
  ORDER_SKIP: '/order_skip',
  ORDER_TAKE: '/order_take'

}

// Create a bot that uses 'polling' to fetch new updates
const _bot = new TelegramBot(token, { polling: true });

// Matches "/echo [whatever]"
_bot.onText(/\/echo (.+)/, (msg, match) => {

  console.log('MSG: ', msg);
  // 'msg' is the received Message from Telegram
  // 'match' is the result of executing the regexp above on the text content
  // of the message

  const chatId = msg.chat.id;
  const resp = match[1]; // the captured "whatever"


  // send back the matched "whatever" to the chat
  _bot.sendMessage(chatId, resp);
});


// Listen for any kind of message. There are different kinds of
// messages.
_bot.on('message', (msg) => {
// const chatId = msg.chat.id;

console.log('MESSAGE: ', msg);

// send a message to the chat acknowledging receipt of their message
// _bot.sendMessage(chatId, 'Received your message');
});

// _bot.onText(_BotCommand.SEND_INFO, msg => {
//   _bot.sendMessage(msg.from.id, `Вас зовут ${msg.from.last_name} ${msg.from.first_name}`)
// });

// _bot.onText(_BotCommand.GREETINGS, msg => {
//   _bot.sendMessage(msg.from.id, `Приветствую Вас, ${msg.from.last_name} ${msg.from.first_name}!`)
// })

// _bot.onText(_BotCommand.CREATE_OFFER, msg => {
//   _bot.sendMessage(msg.from.id, `Пока в разработке...`)
// })

const orderHTML = `
  <b>Новая завяка</b>
  <code>Текст из тэга Code</code>
  <pre>Текст из тэга PRE</pre>
  <a href="www.fasadkrym.ru">Текст из тэга A</a>
`;


_bot.onText(new RegExp('Main menu'), msg => {

  _bot.sendMessage(msg.from.id, 'Главное меню', {
    reply_markup: {
      inline_keyboard: [
        [
          {
            text: 'Привет!',
            callback_data: '/greet_me'
          }, {
            text: 'Кто я?',
            callback_data: '/get_info'
          }, {
            text: 'Создать заявку',
            callback_data: '/create_offer'
          }
        ]
      ]
    }
  })

})


/**
 * queries from inline keyboards
 */
_bot.on('callback_query', query => {

  let answer = '';

  switch (query.data) {

    case _BotCommand.GREETINGS:
        answer = `Приветствую Вас, ${query.from.last_name} ${query.from.first_name}!`
      break;
    case _BotCommand.SEND_INFO:
        answer = `Вас зовут ${query.from.last_name} ${query.from.first_name}`
      break;
    case _BotCommand.CREATE_OFFER:
      answer = `Создан заказ тестовый заказ...`
      break;
    case _BotCommand.ORDER_SKIP:
      answer = `Очередь передана следующему участнику...`
      break;
    case _BotCommand.ORDER_TAKE:
      answer = `Вы забрали заказ...`
      break;
    default:
      answer = `неверная команда`

  }

  _bot.answerCallbackQuery({ callback_query_id: query.id, text: answer, show_alert: true });

  if (query.data === _BotCommand.CREATE_OFFER) {

    _bot.sendMessage(query.from.id, orderHTML, {
      reply_markup: {
        inline_keyboard: [
          [
            {
              text: 'Забрать заказ',
              callback_data: '/order_take'
            }, {
              text: 'Не интересно',
              callback_data: '/order_skip'
            }
          ]
        ]
      },
      parse_mode: 'HTML'
    })

  }

  if (query.data === _BotCommand.ORDER_SKIP) {
    _bot.editMessageText({ inline_keyboard: [], message_id: query.message.message_id })
  }

});

_bot.on('polling_error', error => console.log('ERROR: ', error));




module.exports.startMessage = () => _bot.sendMessage(548286025, 'Откройте меню', {
  reply_markup: {
    keyboard: [
      [
        {
          text: 'Main menu'
        }
      ]
    ]
  }
});


module.exports.sendMessage = async (chat, msg) => {
  return await _bot.sendMessage(chat || fk1, msg, {
    parse_mode: 'HTML'
  })
}

module.exports.startBot = () => {
  _bot.startPolling();
}


/**
 * message object:
 * { message_id: 19,
from:
 { id: ----------,
   is_bot: false,
   first_name: '------',
   last_name: '------',
   language_code: 'ru' },
chat:
 { id: -327784214,
   title: 'FK-1',
   type: 'group',
   all_members_are_administrators: true },
date: 1563866330,
text: '/send-info',
entities: [ { offset: 0, length: 5, type: 'bot_command' } ] }
 */
