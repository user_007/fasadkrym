const { ExtractJwt, Strategy } = require('passport-jwt');
const settings = require('../config/settings');

const opts = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: settings.jwtSecret
}

module.exports = passport => {
  passport.use(
    new Strategy(opts, (payload, done) => {
      const user = payload;

      return done(null, user);
    })
  )
}