const { Bot, Events } = require('viber-bot');

const { viber: { token } } = require('../config/settings');


const _bot = new Bot({
  authToken: token,
  name: 'Фасад Крымыч',
  avatar: '../../static/img/fk_logo.jpg'
})

_bot.on(Events.MESSAGE_RECEIVED, (msg, res) => {
  console.log(msg, res);
});

_bot.on(Events.MESSAGE_SENT, (msg, user) => {
  console.log(msg, user);
})


module.exports = app => {
  app.use('/api/viber', _bot.middleware());
}
