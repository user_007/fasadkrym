let local = {};
const { 
  DB_URI, 
  SESSION_SECRET, 
  JWT_SECRET, 
  CLOUDINARY_NAME, CLOUDINARY_KEY, CLOUDINARY_SECRET, 
  VIBER_API_KEY,
  TELEGRAM_BOT_TOKEN
} = process.env

const isProduction = process.env.NODE_ENV === "production";

if (!isProduction) {
  local = require("../../config/local.settings");
} else {
  
}


module.exports = {
  db: isProduction ? DB_URI : local.db,

  sessionSecret: 
    isProduction ? SESSION_SECRET : local.sessionSecret,

  jwtSecret: 
    isProduction ? JWT_SECRET : local.jwtSecret,

  cloudinary: {

    cloud_name: 
      isProduction ? CLOUDINARY_NAME : local.cloudinary.cloud_name,

    api_key: 
      isProduction ? CLOUDINARY_KEY : local.cloudinary.api_key,

    api_secret: 
      isProduction ? CLOUDINARY_SECRET : local.cloudinary.api_secret

  },

  viber: {
    token: isProduction ? VIBER_API_KEY : local.viber.token
  },

  telegram: {
    token: isProduction ? TELEGRAM_BOT_TOKEN : local.telegram.token,
    fk1: isProduction ? CHAT_FK1 : local.telegram.fk1
  }
};
