const router = require('express').Router();
const uploadMultiformData = require('../middlewares/upload');
const { 
  getListOfUsers,  ensureUserHasAdminRights, quickAddNewUser, manageProfileData, getListOfSpecialities, loginUser, registerNewUser, getUserDataById, addNewUserComment
} = require('../controllers/UserController')

router.get('/', getListOfUsers);

router.get('/:userId', getUserDataById);

router.get('/specializations', getListOfSpecialities);

router.post('/register', registerNewUser);

router.post('/login', loginUser);

router.post('/profile/:userId', manageProfileData);

router.post('/:userId/add-comment', addNewUserComment);

//add ensureUserHasAdminRights middleware before adding user,
//or apply it global to app object to all routes with '/admin/' path
router.post('/admin/user-add', uploadMultiformData, quickAddNewUser);

module.exports = router;