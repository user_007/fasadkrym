const router = require('express').Router();
const { uploadImages } = require('../middlewares/imageProcessing');
const getFiles = require('../middlewares/upload');
const { 
  getWorkStatusList, addNewWork, getWorkCategoriesList, getWorkList 
} = require('../controllers/WorkController');

router.get('/', getWorkList);

router.get('/status-list', getWorkStatusList)

router.get('/cat-list', getWorkCategoriesList);

router.post('/add', getFiles, uploadImages, addNewWork);

module.exports = router;