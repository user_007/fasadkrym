const mongoose = require('mongoose');

const verificationsSchema = new mongoose.Schema({
    email: String,
    token: String,
    created: {
        type: Date,
        default: Date.now,
        expires: '2d'
    }
});

module.exports = mongoose.model('verifications', verificationsSchema);
