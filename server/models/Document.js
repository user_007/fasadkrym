const mongoose = require('mongoose');

const documentSchema = new mongoose.Schema({
    name: String,
    number: String,
    issuedBy: String,
    issueDate: Date,
    expireDate: Date,
    scan: String
});

module.exports = mongoose.model('documents', documentSchema);