const mongoose = require('mongoose');

const workSchema = new mongoose.Schema({
    title: String,
    location: {
        city: {
            type: String,
            default: 'Севастополь'
        },
        address: String
    },
    category: {
        type: String,
        enum: ['Фасадные работы', 'Кровельные работы', 'Высотные работы'],
        default: 'Фасадные работы'
    },
    photo: [String],
    description: String,
    details: {
        dateStart: {
            type: Date,
            default: Date.now
        },
        dateFinish: Date,
        price: {
            type: Number,
            default: 0
        },
        workers: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'users'
            }
        ],
        status: {
            type: String,
            enum: ['подготовка', 'выполнение', 'закрыт'],
            default: 'закрыт'
        }
    }
});

workSchema.statics.getWorkStatusList = () => {
    return workSchema.path('details.status').enumValues;
}

workSchema.statics.getWorkCategoriesList = () => {
    return workSchema.path('category').enumValues;
}

workSchema.statics.validate = workData => {
    const { title, location: { address }, details: { dateFinish: date } } = workData;
    let errors = {};

    if (!title || title.length < 5) {
        errors.title = 'Название должно содержать не менее 5 букв'
    }

    if (!address || address.length < 5) {
        errors.address = 'Укажите адрес объекта'
    }

    if (!date) {
        errors.date = 'Укажите дату окончания рбот на объекте'
    }

    if (Object.keys(errors).length) {
        return { valid: false, errors }
    }

    return { valid: true }
}

module.exports = mongoose.model('works', workSchema)
