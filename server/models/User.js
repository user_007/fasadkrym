const mongoose = require('mongoose');
const { isEmail, isMobilePhone, isAlpha, isAlphanumeric } = require('validator');

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        unique: true
    },    
    firstName: String,
    lastName: String,
    fathersName: String,
    phone: String,
    photo: String,
    citizenship: String,
    specialization: {
        type: String,
        enum: ['фасадчик', 'кровельщик', 'бетонщик', 'сварщик']
    },
    info: String,
    vk: String,
    city: String,
    address: String,
    skills: String,
    documents: {
        passport: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'documents'
        },
        certificates: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'documents'
        }]
    },
    ourRemarks: {
        comments: [{
            text: String,
            status: {
                type: String,
                enum: ['important', 'warning', 'standart'],
                default: 'standart'
            },
            published: {
                type: Date,
                default: Date.now
            }
        }],
        availiableDate: {
            type: Date,
            default: Date.now
        }
    },
    
});

userSchema.statics.validation = userData => {
    errors = {};
    const { email, firstName, fathersName, lastName, phone, ...other } = userData;

    (email && isEmail(email)) ? null : errors.email = 'неверный формат эл. почты';

    (firstName && firstName.length > 2 && isAlpha(firstName, 'ru-RU')) ?
        null : errors.firstName = 'имя должно быть не менее 3 букв и состоять только из букв русского алфавита';

    (lastName && lastName.length > 2 && isAlpha(lastName, 'ru-RU')) ?
        null : errors.lastName = 'фамилии должна быть не менее 3 букв и состоять только из букв русского алфавита';

    if (fathersName && fathersName.length) {
        (fathersName.length < 5 && !isAlpha(fathersName, 'ru-RU')) ?
            errors.fathersName = 'отчество должно быть не менее 5 букв и состоять только из букв русского алфавита' : null;
    }

    (phone && isMobilePhone(phone, 'ru-RU')) ? null : errors.phone = 'неверный формат мобильного телефона';

    if (Object.keys(errors).length) {
        return { valid: false, errors }
    }

    return { valid: true }
}

userSchema.statics.getSpecializationList = async () => {
    return await userSchema.path('specialization').enumValues;
}

userSchema.statics.getAccountListOfRightsAndStatuses = () => {
    const fullList = userSchema.path('accountRightsAndStatuses').enumValues;

    return fullList.filter(stat => stat !== 'superadmin');
}

module.exports = mongoose.model('users', userSchema);
