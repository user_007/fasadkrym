const mongoose = require('mongoose');
const { isAlpha, isAlphanumeric } = require('validator');

const userSchema = new mongoose.Schema({
  login: {
    type: String,
    minlength: 6
  },
  password: {
    type: String,
    minlength: 6
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'users'
  },
  rightsAndStatuses: {
    type: [String],
    enum: ['novice', 'BAN', 'moderator', 'personal', 'admin', 'superadmin', 'partner'],
    default: ['novice']
},
})