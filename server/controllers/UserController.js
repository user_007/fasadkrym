const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const moment = require('moment');

const { transporter } = require('../utils/mail');

const { uploadImagesFromForm: loadPhoto } = require('../middlewares/imageProcessing');

const settings = require('../config/settings');

const User = require('../models/User');
const Verification = require('../models/Verification');


/**
 * method expected to use for adding new user by administrator only
 */
module.exports.quickAddNewUser = async (req, res) => {
    //TODO: implement saving docs later
    const { documents, ...candidate } = req.body;

    try {        
        const userWithCandidatesEmail = await User.findOne({ email: candidate.email });

        if (userWithCandidatesEmail) {
            return res.status(400)
                .json({ 
                    success: false, errors: { email: 'Пользователь с такой почтой уже существует' } 
                })
        }

        const photo = await loadPhoto(req.files);

        candidate.photo = photo[0];

        const newUser = new User({ ...candidate });

        const result = await newUser.save();

        res.status(200).json({ success: true, newUser })

        console.log('Saving candidate result: ', result);
    } catch (ex) {
        console.log('Error occured while trying to quick add of new User: ', ex)
        res.status(400).json({ success: false, error: ex })
    }
}

/**
 * method expected to use for save email (if not exist) 
 * and password of new user into token, 
 * and send verification letter to specifyed email address
 */
module.exports.registerNewUser = async (req, res) => {
    //...
    const { ...candidate } = req.body;

    //check if verification had been already sent
    const verification = await Verification.findOne({ email: candidate.email });

    if (verification) {
        return res.status(400).json({ success: false, errors: { email: `письмо для подтвержления этой почты уже было отправлено ${verification.created}` } })
    }

    const { errors } = User.validation(candidate);
    const registerErrors = {};

    //validation form data
    Object.keys(errors).forEach(errName => {
        Object.keys(candidate).includes(errName) ?
            registerErrors[errName] = errors[errName] : null
    })

    if (Object.keys(registerErrors).length) {
        return res.status(400).json({ success: false, errors: { ...registerErrors } });
    }

    try {
        //hashing password...
        const hashedPassword = await bcrypt.hash(candidate.password.trim(), 10);

        const newUser = {
            ...candidate,
            password: hashedPassword
        };

        //creating token with user data
        const token = jwt.sign(newUser, settings.jwtSecret);

        //saving token to DB
        const newVerification = new Verification({ email: candidate.email, token });
        await newVerification.save();

        //TODO: sending verification letter to specifyed email address with id of Verification document
        //...
        const html = `<p>пока такое... ссылка на документ с токеном ${newVerification._id}</p>`;

        const mailBody = {
            to: candidate.email,
            from: 'fasadkrym.sevastopol@yanex.ru',
            subject: 'Подтверждение почты для аккаунта в базе данных специалистов Fasadkrym',
            html,
        }

        await transporter.sendMail(mailBody);

        //just for test aquired data
        res.json({ success: true, details: { name: candidate.name, email: candidate.email, date: new Date() } });
    } catch (ex) {
        res.status(400).json({ success: false, errors: { encriptionError: ex, stack: ex.stack } });
    }
}


module.exports.loginUser = async (req, res) => {
    const { email, password } = req.body;

    const user = await User
        .findOne({ email })
        .select('-_id -documents -ourRemarks');

    if (!user) {
        //check if verification was sent on specifyed email address
        const thisUserVerification = await Verification.findOne({ email });

        if (thisUserVerification) {
            const { created: date } = thisUserVerification;

            return res
                .status(400)
                .json({ 
                    success: false, 
                    errors: { email: `Ваша почта еще не подтверждена! Письмо с проверкой Вашего адреса эл. почты было отправлено ${moment(date).format('DD-MM-YYY HH:mm')}` } 
                });
        }

        return res.status(404).json({ success: false, errors: { email: 'Пользователь с таким адресом эл. почты не найден' } });
    }

    const passwordIsValid = await bcrypt.compare(password, user.password);

    if (!passwordIsValid) {
        return rea.status(400).json({ success: false, errors: { password: 'Неверный пароль' } });
    }

    const opts = {
        expires: '4h'
    }

    const token = jwt.sign(user, settings.jwtSecret, opts);

    res.json({ success: true, token: `Bearer ${token}` });
}


/**
 * get the list of all usrs from DB
 */
module.exports.getListOfUsers = async (req, res) => {
    try {
        //think what paths we need to populate in User documents
        const userList = await User.find(
            {}
        )
        .select('-password -documents');

        res.status(200).json({ success: true, list: userList })
    } catch (ex) {
        res.status(400).json({ success: false, errors: ex.message })
    }
}

/**
 * method expected to check if user has admin rights/role... 
 * if true, go to next middleware, else return status 403
 */
module.exports.ensureUserHasAdminRights = async (req, res, next) => {
    //...
}

/**
 * method expected to add/update profile data
 */
module.exports.manageProfileData = async (req, res, next) => {
    const { params: { userId }, body: { ...profileData } } = req;

    //todo: validation of profile data

    try {
        const result = await User.findOneAndUpdate(
            { id: userId },
            { ...profileData }
        );

        res.status(200).json({ success: true, result })
    } catch (ex) {
        res.status(400).json({ success: false, errors: { profileUpdate: ex.message } })
    }
}

module.exports.getUserDataById = async (req, res) => {
    const { userId } = req.params;
    
    try {
        const currentUserData = await User.findOne({ _id: userId });

        if (!currentUserData) {
            return res.status(404).json({ success: false, errors: { user: 'пользователь не найден...' } })
        }

        return res.status(200).json({ success: true, currentUserData });
    } catch (ex) {
        console.log(ex)
        return res.status(400).json({ success: false, errors: { user: ex.message } })
    }
}

module.exports.getListOfSpecialities = async (req, res) => {
    const specs = await User.getSpecializationList();

    res.status(200).json(specs);
}

module.exports.addNewUserComment = async (req, res) => {
    const { body: comments, params: { userId } } = req;

    try {

        const result = await User.findOneAndUpdate(
            { _id: userId },
            {
                $set: { 'ourRemarks.comments': comments }
            }
        );

        res.status(200).json({ success: true, comments })
        
    } catch (ex) {
        res.status(400).json({ success: false, errors: { comment: ex.message } })
    }

}
