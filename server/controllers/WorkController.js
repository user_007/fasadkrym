const Work = require('../models/Work');

/**
 * method returns array of allowed names for work processing statuses
 */
module.exports.getWorkStatusList = async (req, res) => {
  const list = await Work.getWorkStatusList();

  res.json(list);
}

module.exports.getWorkCategoriesList = async (req, res) => {
  try {
    const list = await Work.getWorkCategoriesList();

    res.status(200).json({ success: true, list })
  } catch (ex) {
    res.status(400).json({ success: false, errors: { categoriesList: ex } });
  }
}


/**
 * method returns list of all 'works' from DB
 */
module.exports.getWorkList = async (req, res) => {
  const { ...filters } = req.query;
  let conditions = {
    year: { $gte: `01.01.${filters.yearStart}`, $lte: `31.12.${filters.yearEnd}` }
  }

  if (filters.category) {
    conditions.category = filters.category
  }

  const sortMethod = filters.sortMethod.split('-');

  const sortType = sortMethod[1] === 'desc' ? `-${sortMethod[0]}` : sortMethod[0];

  console.log('Object list query from req query: ', filters);  

  try {
    // const workList = await Work.find({
    //   ...conditions
    // })
    // .sort(sortType)
    // .limit(Number(filters.objectsListLimit))
    // .skip(Number(filters.objectsListOffset));

    const workList = await Work.find();

    res.json({ success: true, list: workList });
  } catch (ex) {
    console.log(ex.message)
    res.status(400).json({ success: false, errors: { fetchWorkList: ex.message } });
  }
}


/**
 * add new work
 */
module.exports.addNewWork = async (req, res) => {
  //expected to get files from file upload middleware to req.files before
  let { body: { city, address, date, ...workData }, savedFiles: files } = req;
  workData = {
    ...workData,
    photo: [...files],
    location: {
      city, address
    },
    details: {
      dateFinish: date
    }
  }


  const newWork = new Work({...workData});

  console.log('NEW WORK: ', workData);

  const workValidationResult = Work.validate(newWork);

  if (!workValidationResult.valid) {
    return res.status(400).json({ success: false, errors: workValidationResult.errors })
  }

  try {
    await newWork.save();
    console.log('Jbject was successfully saved to DB')

    res.json({ success: true, newWork });
  } catch (ex) {
    res.status(400).json({ success: false, errors: { saveNewWork: ex.message } })  
  }
}

module.exports.updateWorkData = async (req, res) => {
  let { body: { id, ...data }, files } = req;

  const updateData = {
    ...data,
    photo: files
  }

  try {
    await Work.findOneAndUpdate(
      { _id: id },
      { ...updateData }
    )

    res.json({ success: true });
  } catch (ex) {
    res.status(400).json({ success: false, errors: { workUpdate: ex.message } });
  }
}
