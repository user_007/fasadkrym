const express = require('express');
const app = express();
const path = require('path');

const { sendMessage, startMessage } = require('./middlewares/telegram-bot');

const { dbConnect } = require('./startup/db');


// require('./startup/static')(app);
require('./startup/tools')(app);
require('./middlewares/viber-bot')(app);
require('./startup/routes')(app);


if (process.env.NODE_ENV === 'production') {

    app.use(express.static('client/build'));

    app.get('*', (req, res) => {

        res.sendFile(path.resolve(__dirname, '../client', 'build', 'index.html'))

    })

}


const port = process.env.PORT || 5080;

module.exports.start = async () => {

    try {
        await Promise.all([
            dbConnect(),
            app.listen(port, () => {
                console.log(`server listen on port ${port}...`)

                // sendMessage(null, ``)
            })
        ]);

        startMessage();
        // startBot();
    } catch (ex) {
        console.log('Connection error: ', ex)
    }


};
